package com.ittest.wikitest.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.ittest.wikitest.api.Client
import com.ittest.wikitest.model.Article
import com.ittest.wikitest.model.repository.ArticlesRepository
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class ArticlesViewModel : ViewModel() {

    private val parentJob = Job()

    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Default

    private val scope = CoroutineScope(coroutineContext)

    private val repository : ArticlesRepository = ArticlesRepository(Client.wikiApiClient)

    val articlesLiveData = MutableLiveData<MutableList<Article>>()
    val finishedFetchLiveData = MutableLiveData<Boolean>()

    fun fetchArticles(lat: Double, lon: Double) {
        scope.launch {
            var imageContinue: String? = null
            var startPage = 0
            var lastPage: Int
            do {
                val articles = arrayListOf<Article>()
                val articleResponse = repository.getArticles(lat, lon, imageContinue)
                lastPage = (articleResponse?.cont?.imcontinue?.substringBefore('|')?.toInt() ?: -1).apply {
                    if (this > -1) { imageContinue = "$this|" }
                }
                articleResponse?.query?.pages?.let {
                    for (article in it.values) {
                        if (article.pageid < startPage) continue
                        if (article.pageid >= lastPage) break
                        article.images?.apply {
                            val images = ArrayList<String>(this.size)
                            forEach { ir -> images.add(ir.title) }
                            articles.add(Article(article.pageid, article.title, images))
                        } ?: run { articles.add(Article(article.pageid, article.title, emptyList())) }
                    }
                }
                articlesLiveData.postValue(articles)
                startPage = lastPage
            } while (articleResponse?.cont != null)
            finishedFetchLiveData.postValue(true)
        }
    }

    fun cancelAllRequests() = coroutineContext.cancel()

    companion object {
        private const val TAG = "ArticlesViewModel"
    }
}