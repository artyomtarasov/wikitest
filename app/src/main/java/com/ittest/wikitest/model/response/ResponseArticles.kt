package com.ittest.wikitest.model.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ArticlesResponse(
    @Json(name = "continue")
    val cont: ContinueResponse?,
    val batchcomplete: String?,
    val query: QueryResponse?
)

@JsonClass(generateAdapter = true)
data class ContinueResponse(
    @Json(name = "continue")
    val cont: String,
    val imcontinue: String
)

@JsonClass(generateAdapter = true)
class QueryResponse(
    val pages: Map<String, ArticleResponse>?
)

@JsonClass(generateAdapter = true)
class ArticleResponse(
    val images: List<ImageResponse>?,
    val index: Int,
    val ns: Int,
    val pageid: Int,
    val title: String
)

@JsonClass(generateAdapter = true)
data class ImageResponse(
    val ns: Int,
    val title: String
)