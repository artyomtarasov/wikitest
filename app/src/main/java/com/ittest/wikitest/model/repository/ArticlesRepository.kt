package com.ittest.wikitest.model.repository

import com.ittest.wikitest.api.WikiApi
import com.ittest.wikitest.model.response.ArticlesResponse

class ArticlesRepository(private val api : WikiApi) : BaseRepository() {
    suspend fun getArticles(lat: Double, lon: Double, imageContinue: String?) : ArticlesResponse? {
        return safeApiCall(
            call = {
                api.getArticlesAsync(
                    coordinates = "$lat|$lon",
                    cont = if (imageContinue == null) null else "||",
                    imageContinue = imageContinue
                ).await()
            },
            errorMessage = "Error Fetching Articles"
        )
    }
}