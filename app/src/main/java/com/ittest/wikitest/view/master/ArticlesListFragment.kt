package com.ittest.wikitest.view.master

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.ittest.wikitest.R
import com.ittest.wikitest.base.BaseFragment
import com.ittest.wikitest.model.Article
import com.ittest.wikitest.view.WikiListActivity
import com.ittest.wikitest.viewmodel.ArticlesViewModel
import kotlinx.android.synthetic.main.fragment_articles_list.view.*

class ArticlesListFragment : BaseFragment(), ArticlesListAdapter.OnListItemClickListener {

    private lateinit var mActivity: WikiListActivity
    private lateinit var mArticlesViewModel: ArticlesViewModel

    private lateinit var mView: View
    private lateinit var mArticlesRecyclerView: RecyclerView
    private lateinit var mProgress: ProgressBar
    private lateinit var mLayoutManager: RecyclerView.LayoutManager
    private lateinit var mStatusText: TextView

    private lateinit var mArticlesListAdapter: ArticlesListAdapter
    private lateinit var mLocationManager : LocationManager
    private var mLongitude: Double = 0.0
    private var mLatitude: Double = 0.0

    private val mArticlesList = ArrayList<Article>()

    private val mLocationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            Log.d(TAG, "Longitude: ${location.longitude}; Latitude: ${location.latitude}")
            mLongitude = location.longitude
            mLatitude = location.latitude
            mStatusText.text = getText(R.string.obtaining_articles)

            mArticlesViewModel.fetchArticles(mLatitude, mLongitude)

            mArticlesViewModel.articlesLiveData.observe(this@ArticlesListFragment, Observer {
                if (it != null && it.size > 0) {
                    Log.d(TAG, "Fetched new ${it.size} articles")
                    mArticlesList.addAll(it)
                    initializeAdapter()
                    mArticlesListAdapter.setData(mArticlesList)
                }
            })

            mArticlesViewModel.finishedFetchLiveData.observe(this@ArticlesListFragment, Observer {
                if (it == true) {
                    stopLocationUpdates()
//                    Toast.makeText(
//                        mActivity,
//                        "Finished fetching! Found ${mArticlesList.size} total articles",
//                        Toast.LENGTH_LONG
//                    ).show()

                    for (article in mArticlesList) {
                        Log.d(TAG, "$article")
                    }
                }
            })
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mActivity = context as WikiListActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        mLocationManager = mActivity.getSystemService(AppCompatActivity.LOCATION_SERVICE) as LocationManager

        mArticlesViewModel = ViewModelProviders.of(this).get(ArticlesViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(
            R.layout.fragment_articles_list,
            container,
            false
        )

        initViews()

        return mView
    }

    private fun initViews() {
        with(mView) {
            mArticlesRecyclerView = fragment_list_articles_list
            mProgress = fragment_list_progress
            mStatusText = fragment_list_text
            mLayoutManager = LinearLayoutManager(mActivity)
            mArticlesRecyclerView.layoutManager = mLayoutManager
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        startLocationUpdates()
    }

    private fun startLocationUpdates() {
        if (
            ActivityCompat.checkSelfPermission(
                mActivity, Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(
                mActivity, Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            mStatusText.text = getText(R.string.looking_for_you)
            mLocationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER,
                0L,
                0f,
                mLocationListener
            )
        } else {
            mActivity.checkPermissions()
        }
    }

    private fun initializeAdapter() {
        if (!::mArticlesListAdapter.isInitialized) {
            mArticlesListAdapter = ArticlesListAdapter(mActivity, this)
            mArticlesRecyclerView.adapter = mArticlesListAdapter

            mActivity.mRefreshMenuItem.isVisible = true
            mArticlesRecyclerView.visibility = View.VISIBLE
            mProgress.visibility = View.GONE
            mStatusText.visibility = View.GONE
        }
    }

    private fun stopLocationUpdates() {
        mLocationManager.removeUpdates(mLocationListener)
    }

    override fun onItemClicked(article: Article) {
        mActivity.startDetailFragment(article)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            when (item.itemId) {
                R.id.try_again -> {
                    mArticlesListAdapter.clear()
                }
                else -> return false
            }
        }
        return true
    }

    companion object {

        private const val TAG = "ArticlesListFragment"

        @JvmStatic
        fun newInstance() = ArticlesListFragment()
    }
}
