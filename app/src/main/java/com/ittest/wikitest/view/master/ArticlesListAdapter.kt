package com.ittest.wikitest.view.master

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.ittest.wikitest.R
import com.ittest.wikitest.model.Article
import kotlinx.android.synthetic.main.item_fragment_articles_list.view.*

class ArticlesListAdapter(
    private val context: Context,
    private val listener: OnListItemClickListener
) : RecyclerView.Adapter<ArticlesListAdapter.ViewHolder>() {

    private lateinit var mView: View
    private lateinit var mViewHolder: ViewHolder

    private lateinit var articlesList: ArrayList<Article>

    override fun getItemCount(): Int {
        return articlesList.size
    }

    fun setData(articles: ArrayList<Article>) {
        articlesList = articles
        notifyDataSetChanged()
    }

    fun clear() {
        articlesList.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mView = LayoutInflater.from(context).inflate(
            R.layout.item_fragment_articles_list,
            parent,
            false
        )

        mViewHolder = ViewHolder(mView)
        return mViewHolder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val article = articlesList[position]
        holder.article = article
        holder.titleTextView.text = article.title
        article.images.let {
            holder.imagesCountTextView.text = context.getString(R.string.total_images_placeholder, it.size)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val view = itemView
        val titleTextView: TextView = itemView.articles_list_item_title
        val imagesCountTextView: TextView = itemView.articles_list_item_images_count
        lateinit var article: Article

        init {
            this.view.setOnClickListener { listener.onItemClicked(this.article) }
        }
    }

    interface OnListItemClickListener {
        fun onItemClicked(article: Article)
    }
}