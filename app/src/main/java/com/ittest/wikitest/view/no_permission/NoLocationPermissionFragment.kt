package com.ittest.wikitest.view.no_permission

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.ittest.wikitest.R
import com.ittest.wikitest.view.WikiListActivity
import com.ittest.wikitest.base.BaseFragment
import com.ittest.wikitest.view.master.ArticlesListFragment
import kotlinx.android.synthetic.main.fragment_no_location_permissions.view.*

class NoLocationPermissionFragment : BaseFragment() {

    private lateinit var mActivity: WikiListActivity

    private lateinit var mView: View
    private lateinit var mGrantPermissionsButton: Button

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mActivity = context as WikiListActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(
            R.layout.fragment_no_location_permissions,
            container,
            false
        )

        //mActivity.currentFragmentTag = TAG

        initViews()

        return mView
    }

    private fun initViews() {
        with(mView) {
            mGrantPermissionsButton = fragment_restricted_grant_permissions_button
        }
        mGrantPermissionsButton.setOnClickListener { requestPermissions() }
    }

    private fun requestPermissions() {
        if (
            ActivityCompat.checkSelfPermission(
                mActivity, Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(
                mActivity, Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                mActivity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATIONS_PERMISSION_REQUEST
            )
        } else {
            mActivity.mRefreshMenuItem.isVisible = false
            mActivity.startFragment(ArticlesListFragment.newInstance())
        }
    }

    companion object {
        const val LOCATIONS_PERMISSION_REQUEST = 100
        const val TAG = "NoLocationPermissionFragment"

        @JvmStatic
        fun newInstance() = NoLocationPermissionFragment()
    }
}