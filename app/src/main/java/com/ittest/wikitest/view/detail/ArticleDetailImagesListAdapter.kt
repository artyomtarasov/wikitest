package com.ittest.wikitest.view.detail

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.ittest.wikitest.R

class ArticleDetailImagesListAdapter(
    private var context: Context,
    private var imagesList: List<String>
) : RecyclerView.Adapter<ArticleDetailImagesListAdapter.ViewHolder>() {

    private lateinit var mView: View
    private lateinit var mViewHolder: ViewHolder

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        mView = LayoutInflater.from(context).inflate(
            R.layout.item_fragment_article_detailed_images,
            parent,
            false
        )

        mViewHolder = ViewHolder(mView)
        return mViewHolder
    }

    override fun getItemCount(): Int {
        return imagesList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = imagesList[position]
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var title: TextView = itemView.findViewById(R.id.fragment_article_detailed_item_title)
    }
}