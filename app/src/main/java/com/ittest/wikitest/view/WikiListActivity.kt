package com.ittest.wikitest.view

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import com.ittest.wikitest.R
import com.ittest.wikitest.base.BaseFragment
import com.ittest.wikitest.model.Article
import com.ittest.wikitest.view.detail.ArticleDetailFragment
import com.ittest.wikitest.view.master.ArticlesListFragment
import com.ittest.wikitest.view.no_permission.NoLocationPermissionFragment
import com.ittest.wikitest.view.no_permission.NoLocationPermissionFragment.Companion.LOCATIONS_PERMISSION_REQUEST
import kotlinx.android.synthetic.main.activity_wiki_list.*

class WikiListActivity : AppCompatActivity() {

    private lateinit var mToolbar: Toolbar
    lateinit var mRefreshMenuItem: MenuItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wiki_list)

        mToolbar = toolbar
        setSupportActionBar(mToolbar)

        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            LOCATIONS_PERMISSION_REQUEST
        )
    }

    fun checkPermissions() {
        if (
            ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            mRefreshMenuItem.isVisible = true
            startFragment(NoLocationPermissionFragment.newInstance())
        } else {
            mRefreshMenuItem.isVisible = false
            startFragment(ArticlesListFragment.newInstance())
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        mRefreshMenuItem = menu!!.findItem(R.id.try_again)

        mRefreshMenuItem.isVisible = false

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            when (item.itemId) {
                mRefreshMenuItem.itemId -> {
                        finish()
                        startActivity(Intent(this, WikiListActivity::class.java))
                    }
                }
            }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == LOCATIONS_PERMISSION_REQUEST) {
            checkPermissions()
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    fun startFragment(fragment: BaseFragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.main_frame, fragment)
            .commitAllowingStateLoss()
    }

    fun startDetailFragment(article: Article) {
        supportFragmentManager.beginTransaction()
            .add(R.id.main_frame, ArticleDetailFragment.newInstance(article))
            .addToBackStack(null)
            .commitAllowingStateLoss()

        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setDisplayShowHomeEnabled(false)
    }

    override fun onSupportNavigateUp(): Boolean {
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setDisplayShowHomeEnabled(false)
        onBackPressed()
        return true
    }
}