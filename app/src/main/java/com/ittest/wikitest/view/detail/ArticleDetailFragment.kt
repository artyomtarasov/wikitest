package com.ittest.wikitest.view.detail

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.ittest.wikitest.R
import com.ittest.wikitest.base.BaseFragment
import com.ittest.wikitest.model.Article
import com.ittest.wikitest.view.WikiListActivity

class ArticleDetailFragment : BaseFragment() {

    private lateinit var mActivity: WikiListActivity

    private lateinit var mView: View
    private lateinit var mTitleTextView: TextView
    private lateinit var mSubtitleTextView: TextView
    private lateinit var mImageList: RecyclerView
    private lateinit var mLayoutManager: RecyclerView.LayoutManager
    private lateinit var mImageListListAdapter: ArticleDetailImagesListAdapter

    private lateinit var mArticle: Article

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mActivity = context as WikiListActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            mArticle = it.getParcelable(ARG_ARTICLE) ?: throw IllegalArgumentException("Wrong article selected. Null?")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(
            R.layout.fragment_article_detailed,
            container,
            false
        )

        initViews()
        setContent()

        return mView
    }

    private fun initViews() {
        with(mView) {
            mTitleTextView = findViewById(R.id.fragment_detailed_title)
            mSubtitleTextView = findViewById(R.id.fragment_detailed_subtitle)
            mImageList = findViewById(R.id.fragment_detailed_images_list)
            mLayoutManager = LinearLayoutManager(mActivity)
        }
    }

    private fun setContent() {
        mImageList.layoutManager = mLayoutManager
        mTitleTextView.text = mArticle.title
        mArticle.images.let {
            // Total images: N
            mSubtitleTextView.text = mActivity.getString(R.string.total_images_placeholder, it.size)
        }
        initializeAdapter()
    }

    private fun initializeAdapter() {
        mArticle.images.let {
            mImageListListAdapter = ArticleDetailImagesListAdapter(mActivity, it)
        }
        mImageList.adapter = mImageListListAdapter
    }

    companion object {
        private const val TAG = "ArticleDetailFragment"
        private const val ARG_ARTICLE = "article"

        @JvmStatic
        fun newInstance(article: Article): ArticleDetailFragment {
            val fragment = ArticleDetailFragment()
            val args = Bundle()
            args.putParcelable(ARG_ARTICLE, article)
            fragment.arguments = args

            return fragment
        }
    }
}
