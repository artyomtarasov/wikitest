package com.ittest.wikitest.api

import com.ittest.wikitest.model.response.ArticlesResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface WikiApi {
    @GET("/w/api.php?")
    fun getArticlesAsync(
        @Query("action") action: String = "query",
        @Query("format") format: String = "json",
        @Query("generator") generator: String = "geosearch",
        @Query("imlimit") imlimit: String = "max",
        @Query("ggscoord") coordinates: String,
        @Query("ggsradius") radius: String = "10000",
        @Query("ggslimit") limit: String = "50",
        @Query("prop") prop: String = "images",
        @Query("continue") cont: String? = null,
        @Query("imcontinue") imageContinue: String? = null
    ): Deferred<Response<ArticlesResponse>>
}